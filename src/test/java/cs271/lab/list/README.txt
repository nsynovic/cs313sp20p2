Nicholas Synovic
COMP 313

Filename : TestIterator.java
Questions are numbered in the order that they appear.

1)	Switching list to a LinkedList<Integer> did not change the code behaviorally.
2)	If list.remove(Integer.valueOf(77)) is used over list.remove(), then the 77th index of list is removed every time i.next() == 77.

Filename : TestList.java
Questions are numbered in the order that they appear.

1)	Switching list to a LinkedList<Integer> did change the code behaviorally. More errors were detected in the test methods than the initial run through.
2)	list.remove removes the 5th index of list. In this case, the third 77th is removed from the list.
3)	list.remove(Integer.valueOf(5)) removes the 5th index of list. In this case, the 6 is removed from the list.

Filename : TestPerformance.java
Questions are numbered in the order that they appear.

1)	SIZE = 10, REPS = 100,000,000: testArrayListAccess = 0.203s, testArrayListAddRemove = 3.125s, testLinkedListAccess = 0.446s, testLinkedListAddRemove = 0.886s
	SIZE = 100, REPS = 100,000,000: testArrayListAccess = 0.160s, testArrayListAddRemove = 2.747s, testLinkedListAccess = 1.435s, testLinkedListAddRemove = 0.916s
	SIZE = 1000, REPS = 100,000,000:  testArrayListAccess = 0.184s, testArrayListAddRemove = 15.818s, testLinkedListAccess = 27.555s, testLinkedListAddRemove = 1.061s
	SIZE = 10000, REPS = 100,000,000:  testArrayListAccess = 0.196s, testArrayListAddRemove = 1m58.80s, testLinkedListAccess = 6m21.89s, testLinkedListAddRemove = 0.918s
	
	It is clear after the fourth run through that ArraryList performs better than a LinkedList does in these tests in terms of performance.	
	